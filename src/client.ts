class Logger {

  log (message: string) {
    console.log('[~]: ', message)
  }

  error (message: string) {
    console.log('[!]: ', message)
  }

  msg (message: string) {
    console.log(message)
  }

}

class Url {

  value: string

  constructor (url: string) {
    // TODO: create validator
    this.value = url
  }
}

class Client {

  wsClient: any
  logger: Logger

  constructor(url: Url, options: object) {
    let ws = require('ws');

    this.wsClient = new ws(url, options)
    this.logger = new Logger

    this.wsClient.on('open', () => {
      this.logger.log('connected')
      this.wsClient.send('something')
    })

    this.wsClient.on('message', (data: any) => {
      this.logger.msg(data)
    })

    this.wsClient.on('close', () => {
      this.logger.log('disconnected.')
    })
  }

}

const url = new Url('ws://localhost:4000')
const optiosn = {
  perMessageDeflate: false
}

let client = new Client(url, options)