const options = {
  port: 4000,
  perMessageDeflate: {
    zlibDeflateOptions: { // See zlib defaults.
      chunkSize: 1024,
      memLevel: 7,
      level: 3,
    },
    zlibInflateOptions: {
      chunkSize: 10 * 1024
    },
    // Other options settable:
    clientNoContextTakeover: true, // Defaults to negotiated value.
    serverNoContextTakeover: true, // Defaults to negotiated value.
    clientMaxWindowBits: 10,       // Defaults to negotiated value.
    serverMaxWindowBits: 10,       // Defaults to negotiated value.
    // Below options specified as default values.
    concurrencyLimit: 10,          // Limits zlib concurrency for perf.
    threshold: 1024,               // Size (in bytes) below which messages
                                   // should not be compressed.
  }
}

class Server {
  wsServer: any

  constructor(options: object) {
    let ws = require('ws');

    this.wsServer = new ws.Server(options)

    this.wsServer.broadcast = (data: any) => {
      this.wsServer.clients.forEach((client: any) => {
        if (client.readyState === WebSocket.OPEN) {
          client.send(data)
        }
      })
    }

    this.wsServer.on('connection', (connection: any) => {
      connection.on('message', (data: any) => {
        // Broadcast to everyone else.
        this.wsServer.clients.forEach((client: any) => {
          if (client !== connection && client.readyState === WebSocket.OPEN) {
            client.send(data)
          }
        })
      })
    })
  }
}

let server = new Server(options)