# ws-chat-typescript

Simple chat on web-socket for typescript

# Build

```shell
git clone https://gitlab.com/Drake.aren/ws-chat-typescript
cd ws-chat-typescript
npm run build
```

# Usage

run `npm run start:clinet` or `npm run start:server`